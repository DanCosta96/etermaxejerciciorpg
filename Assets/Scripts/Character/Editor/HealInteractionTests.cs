﻿using NUnit.Framework;
using System;
public class HealInteractionTests : CharacterTests
{
    [SetUp]
    public void SetUp()
    {
        GivenACharacter(FighterTypes.Melee);
        GivenATarget(FighterTypes.Melee);
    }

    [Test]
    public void HealthCannotGoBelow0()
    {
        character.Damage(target, MaxHealth * 2);

        ThenHealthIs(target, 0);
    }

    [Test]
    public void CanHealACharacter()
    {
        GivenHealth(character, 50);

        character.Heal(character, 10);

        ThenHealthIs(character, 60);
    }

    [Test]
    public void CannotHealADeadCharacter()
    {
        GivenHealth(character, 0);
        GivenAlive(character, false);

        character.Heal(character, 10);

        ThenHealthIs(character, 0);
    }

    [Test]
    public void HealthCannotBeRaiseAbove1000()
    {
        character.Heal(character, 100);

        ThenHealthIs(character, MaxHealth);
    }

    [Test]
    public void CannotHealOtherCharacterThanHimself()
    {
        Assert.Throws<ArgumentException>(() => character.Heal(target, Ten));
    }
}