﻿using NUnit.Framework;
using System;

public class DamageInteractionTests : CharacterTests
{
    [SetUp]
    public void SetUp()
    {
        GivenACharacter(FighterTypes.Melee);
        GivenATarget(FighterTypes.Melee);
    }

    [Test]
    [TestCase(10)]
    [TestCase(100)]
    [TestCase(133)]
    public void CanDealDamageToCharacter(int damage)
    {
        character.Damage(target, damage);

        ThenHealthIs(target, MaxHealth - damage);
    }

    [Test]
    public void CannotDealDamageToHimself()
    {
        Assert.Throws<ArgumentException>(() => character.Damage(character, 100));
    }

    [Test]
    public void HealthCannotGoBelow0()
    {
        character.Damage(target, MaxHealth * 2);

        ThenHealthIs(target, 0);
    }

    [Test]
    public void DiesWhenHealthIs0()
    {
        character.Damage(target, MaxHealth);

        ThenAliveIs(target, false);
    }

    [Test]
    [TestCase(6, HalfHealth)]
    [TestCase(5, 0)]
    [TestCase(7, HalfHealth)]
    public void CharacterWith5LevelsBelowTargetDamageDealedIsReduced50Percent(int targetLevel, int ExpectedHealth)
    {
        GivenLevel(target, targetLevel);

        character.Damage(target, MaxHealth);

        ThenHealthIs(target, ExpectedHealth);
    }

    [Test]
    [TestCase(6, QuarterHealth)]
    [TestCase(5, HalfHealth)]
    [TestCase(7, QuarterHealth)]
    public void CharacterWith5LevelsAboveTargetDo50PercentMoreDamage(int characterLevel, int ExpectedHealth)
    {
        GivenLevel(character, characterLevel);

        character.Damage(target, HalfHealth);

        ThenHealthIs(target, ExpectedHealth);
    }

    [Test]
    [TestCase(FighterTypes.Melee,2,MaxHealth-Ten)]
    [TestCase(FighterTypes.Melee,1,MaxHealth-Ten)]
    [TestCase(FighterTypes.Melee,3,MaxHealth)]
    [TestCase(FighterTypes.Ranged,19,MaxHealth - Ten)]
    [TestCase(FighterTypes.Ranged,20,MaxHealth - Ten)]
    [TestCase(FighterTypes.Ranged,21,MaxHealth)]
    public void OnlyCanAttackIfTargetIsInRange(FighterTypes type, int TargetPosition, int ExpectedHealth)
    {
        GivenACharacter(type);

        target.Position = TargetPosition;
        character.Damage(target, Ten);

        ThenHealthIs(target, ExpectedHealth);

    }
}
