﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class SetUpTests : CharacterTests
{
    [SetUp]
    public void SetUp()
    {
        GivenACharacter(FighterTypes.Melee);
    }
    [Test]
    public void HealthIs1000WhenCreated()
    {
        ThenHealthIs(character, MaxHealth);
    }

    [Test]
    public void LevelIs1WhenCreated()
    {
        ThenLevelIs(character, 1);
    }

    [Test]
    public void AliveTrueWhenCreated()
    {
        ThenAliveIs(character, true);
    }

    [Test]
    public void HasAttackRange()
    {
        ThenAttackRangeIs(character, 2);
    }

    [Test]
    public void MeleeHas2AttackRange()
    {
        GivenACharacter(FighterTypes.Melee);

        ThenAttackRangeIs(character, 2);
    }

    [Test]
    public void RangedHas20AttackRange()
    {
        GivenACharacter(FighterTypes.Ranged);

        ThenAttackRangeIs(character, 20);
    }
}
