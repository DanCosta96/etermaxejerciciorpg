﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;

public class CharacterTests
{
    public Character character;
    public Character target;
    public const int MaxHealth = 1000;
    public const int HalfHealth = 500;
    public const int QuarterHealth = 250;
    public const int Ten = 10;


    public void GivenACharacter(FighterTypes type)
    {
        character = new Character(type);
    }

    public void GivenATarget(FighterTypes type)
    {
        target = new Character(type);
    }

    public void GivenLevel(Character character, int level)
    {
        character.Stats.Level = level;
    }

    public void GivenHealth(Character character, int health)
    {
        character.Health.CurrentHealth = health;
    }

    public void GivenAlive(Character character, bool alive)
    {
        character.Health.Alive = alive;
    }

    public void ThenHealthIs(Character character, int health)
    {
        Assert.That(character.Health.CurrentHealth, Is.EqualTo(health));
    }

    public void ThenLevelIs(Character character, int level)
    {
        Assert.That(character.Stats.Level, Is.EqualTo(level));
    }

    public void ThenAliveIs(Character character, bool alive)
    {
        Assert.That(character.Alive, Is.EqualTo(alive));
    }

    public void ThenAttackRangeIs(Character character, int attackRange)
    {
        Assert.That(character.Stats.AttackRange, Is.EqualTo(attackRange));
    }
}
