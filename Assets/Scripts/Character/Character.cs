﻿using System;
using UnityEngine;

public class Character
{
    public int Position = 0;
    private Stats stats;
    private Health health;
    public Health Health => health;
    public bool Alive => Health.Alive;
    public Stats Stats => stats;

    public Character(FighterTypes type)
    {
        stats = new Stats(type);
        health = new Health(1000);
    }

    public void Heal(Character target, int amount)
    {
        HealProcessor.DoHeal(this, target, amount);
    }

    public void Damage(Character target, int amount)
    {
        DamageProcessor.DoDamage(this, target, amount);
    }
}
