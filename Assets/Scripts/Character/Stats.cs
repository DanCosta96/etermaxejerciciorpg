﻿using System;
using UnityEngine;

public class Stats
{
    public FighterTypes FighterType;
    public int Level;
    public int AttackRange = 2;

    public Stats(FighterTypes type)
    {
        Level = 1;
        FighterType = type;
        AttackRange = GetRange();
    }

    private int GetRange()
    {
        switch (FighterType)
        {
            case FighterTypes.Melee:
                return 2;
            case FighterTypes.Ranged:
                return 20;
            default:
                throw new ArgumentException("Fighter Type No Implemented");
        }
    }
}
