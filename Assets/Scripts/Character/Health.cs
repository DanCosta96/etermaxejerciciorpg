﻿public class Health
{
    public int MaxHealth = 1000;
    public int CurrentHealth;
    public bool Alive = true;

    public Health(int maxHealth)
    {
        MaxHealth = maxHealth;
        CurrentHealth = MaxHealth;
    }

    public void SubstractHealth(int amount)
    {
        CurrentHealth -= amount;
        if (CurrentHealth < 0) CurrentHealth = 0;
        if (CurrentHealth == 0) Alive = false;
    }

    public void AddHealth(int amount)
    {
        CurrentHealth += amount;
        if (CurrentHealth > MaxHealth) CurrentHealth = MaxHealth;
    }

}

