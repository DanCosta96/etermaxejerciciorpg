﻿using System;
using UnityEngine;

public static class DamageProcessor
{
    public const int LevelDifferenceToModifyAttack = 5;
    public const float HigherLevelDamageMultipier = 1.5f;
    public const float LowerLevelDamageMultipier = .5f;
    public const float NormalDamageMultipier = 1f;

    public static void DoDamage(Character caster, Character target, int amount)
    {
        if (!CanBeDamaged(caster, target))
            return;
        int amountToDeal = CalculateDamage(caster, target, amount);
        target.Health.SubstractHealth(amountToDeal);
    }

    public static bool CanBeDamaged(Character caster, Character target)
    {
        if (caster == target)
            throw new ArgumentException("A Character can not damage himself");
        return TargetInRange(caster, target);
    }

    static int CalculateDamage(Character caster, Character target, int amount)
    {
        int levelDifference = caster.Stats.Level - target.Stats.Level;
        float damageMultiplier = NormalDamageMultipier;

        if (levelDifference >= LevelDifferenceToModifyAttack)
            damageMultiplier = HigherLevelDamageMultipier;
        else if (levelDifference <= -LevelDifferenceToModifyAttack)
            damageMultiplier = LowerLevelDamageMultipier;

        return Mathf.RoundToInt(amount * damageMultiplier);
    }

    private static bool TargetInRange(Character caster,Character target)
    {
        return Mathf.Abs(caster.Position - target.Position) <= caster.Stats.AttackRange;
    }
}
