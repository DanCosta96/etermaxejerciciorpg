﻿using System;

public static class HealProcessor
{
    public static void DoHeal(Character caster, Character target, int amount)
    {
        if (!CanBeHealed(caster,target))
            return;
        target.Health.AddHealth(amount);
    }
    private static bool CanBeHealed(Character caster, Character target)
    {
        if (caster != target)
            throw new ArgumentException("A Character can only heal himself");
        return target.Alive;
    }
}
